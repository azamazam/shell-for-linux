#include <fcntl.h>
#include<stdio.h>
#include<stdlib.h>
#include<unistd.h>
#include<string.h>
#include<sys/wait.h>
#include<sys/types.h>
#include <sys/stat.h>

#define MAX_HIST_COUNT 30
#define MAX_HIST_LINE_LEN 15

#define MAX_LEN 512
#define MAXARGS 15
#define ARGLEN 30
#define PROMPT "AZAM:- "

char* read_cmd(char* ,FILE *);
char** tokenize(char*);
int execute(char** );

int checkIfInternalcmnd(char*);
void printHelp(char *cmd);
int RedirectIO(char**);
void showHistory(char**,int );
int main()
{

  char *cmndline;
  char** arglist;
  int j;
  int s=-1;
  char* prompt=PROMPT;
char **history = (char**)malloc(sizeof(char*)*(MAX_HIST_COUNT));
  while((cmndline = read_cmd(prompt,stdin)) != NULL)
  {

    char *cmnds= strtok(cmndline,";");
    while(cmnds!=NULL)
    {
     history[++s] = (char*)malloc(sizeof(char)*MAX_HIST_LINE_LEN);
      bzero(history[s],MAX_HIST_LINE_LEN);
        if(strcmp(cmnds,"!n")==0){
        
            strcpy(history[s],"!n");
            showHistory(history,s);
        }
     else if((arglist = tokenize(cmnds)) != NULL)
      {
       strcpy(history[s],cmnds);
        execute(arglist);
      }
      cmnds = strtok(NULL,";");
    }

	//free memory
	if (arglist!=NULL &&arglist[0]!=NULL){
      for(j=0;j<MAXARGS;j++)
  	    free(arglist[j]);
      free(arglist);
      free(cmndline);
    }
  }
  printf("\n");
  return 0;

}
char* read_cmd(char* prompt,FILE* fp)
{
  printf("%s",prompt);
  char* cmdline = (char*)malloc(sizeof(char)*MAX_LEN);
  int c=0;
  int pos = 0;
  while((c=getc(fp)) !=EOF)
  {

    if(c== '\n')
    break;
    cmdline[pos++] =c;
  }
  if(c==EOF && pos==0)
  return NULL;
  cmdline[pos] = '\0';
  return cmdline;
}
char **tokenize(char* cmdline)
{
  char **arglist = (char**)malloc(sizeof(char*)*MAXARGS);
  int i;
  for(i=0;i<MAXARGS;i++)
  {
    arglist[i] = (char*)malloc(sizeof(char)*ARGLEN);
    bzero(arglist[i],ARGLEN);
  }
  if(cmdline[0]=='\0')
  return NULL;
  char* cp = cmdline;
  char * start;
  int len;
  int argnum = 0;

  while(*cp != '\0')
  {
    while(*cp ==' ' || *cp == '\t')
    cp++;

    start = cp;
    len = 1;
    while(*++cp != '\0' && !(*cp == ' ' || *cp == '\t'))
    len++;

    strncpy(arglist[argnum],start,len);
    arglist[argnum][len] = '\0';

    argnum++;
  }
  arglist[argnum] = NULL;
  return arglist;
}
int execute(char* arglist[])
{
	int rv = RedirectIO(arglist);
	if(rv==0)
	{
		return 0;
	}
  int statusflag=checkIfInternalcmnd(arglist[0]);
  if(statusflag==-1)
  {
    int cpid = fork();
    switch(cpid)
    {

      case -1:
      perror("fork failed");
      exit(1);
      case 0:
      execvp(arglist[0],arglist);
      perror("exec failed");
      exit(1);
      default:
      waitpid(cpid,&statusflag,0);
      printf("child exitted with status %d\n",statusflag>>8);
      return 0;
    }
  }
  else
  {
    if(statusflag==3) printHelp(arglist[1]);
    if(statusflag==1) exit(0);
    if(statusflag==2)
    {
      int rev =  chdir(arglist[1]);
      if(rev==-1)
      {
        fprintf(stderr, "Action cant be  done\n");
      }
    }
  }
}
int checkIfInternalcmnd(char * comand)
{
  if(strcmp(comand,"exit")==0) return 1;
  else if(strcmp(comand,"cd")==0) return 2;
  else if(strcmp(comand,"help")==0) return 3;
  else return -1;
}
void printHelp(char *cmnd)
{
		//exit help
	if(strcmp(cmnd,"exit")==0)
 	printf("exit: exit [n]\n\tExit the shell.\n\n\tExits the shell with a status of N.  If N is omitted, the exit status is that of the last command executed.\n");

	if(strcmp(cmnd,"cd")==0) //cd help
	{
    	printf("cd: cd [-L|[-P [-e]] [-@]] [dir]\n\t\tChange the shell working directory.\n");
    	printf("\t\tChange the current directory to DIR.  The default DIR is the value of the HOME shell variable.\n");
  	}

}
int RedirectIO(char* arglist[])
{

	int i=0;
	for(i=0;arglist[i]!=NULL;i++)
	{
		if(strcmp(arglist[i],"<")==0 || strcmp(arglist[i],"0<")==0)
		{
			
			int saved_stdin = dup(0);
			int saved_stdout = dup(1);
			close(0);
			int fd= open(arglist[i+1],O_RDONLY);	
				for(int j=i;arglist[j]!=NULL;j++)
					{
							if(strcmp(arglist[j],">")==0 || strcmp(arglist[j],"1>")==0)
							{
								close(1);
								int fd2= open(arglist[j+1],O_WRONLY | O_CREAT | O_TRUNC);
							}
					}
			if(fork()==0)
				{
					
					execlp(arglist[0],"cat",NULL);
				}
			else{
				wait(NULL);
				
				dup2(saved_stdin,0);
				dup2(saved_stdout,1);
				close(saved_stdout);
				close(saved_stdin);
				return 0;
			    }
			
			
		}
		else if(strcmp(arglist[i],">")==0 || strcmp(arglist[i],"1>")==0)
		{
			
			int saved_stdin = dup(0);
			int saved_stdout = dup(1);
			close(1);
			int fd= open(arglist[i+1],O_WRONLY| O_CREAT | O_TRUNC);
	
				for(int  j=i;arglist[j]!=NULL;j++)
					{
						if(strcmp(arglist[j],"<")==0 || strcmp(arglist[j],"0<")==0)
							{
								close(0);
								int fd2= open(arglist[j+1],O_RDONLY);
							}
					}
			if(fork()==0)
				{
				
					execlp(arglist[0],"cat",NULL);
				}
			else{
				wait(NULL);
				
				dup2(saved_stdin,0);
				dup2(saved_stdout,1);
				close(saved_stdout);
				close(saved_stdin);
				return 0;
			    }
		}
	}
	return 1;
}
void showHistory(char* history[],int s)
{
  printf("\n");
  if(s>0)
  for(int i = 0; i<s;i++)
  {
    printf("%d  %s\n",i, history[i]);
  }
  else printf("!n: there is no history\n");
}
