#include <fcntl.h>
#include<stdio.h>
#include<stdlib.h>
#include<unistd.h>
#include<string.h>
#include<sys/wait.h>
#include<sys/types.h>
#include <sys/stat.h>


#define MAX_LEN 512
#define MAXARGS 10
#define ARGLEN 30
#define PROMPT "AZAM:- "

char* read_cmd(char* ,FILE *);
char** tokenize(char*);
int execute(char* arglist[]);

int checkIfInternalcmnd(char*);
void printHelp(char *cmd);
int RedirectIO(char* arglist[]);
int main()
{

  char *cmdline;
  char** arglist;
  int j;
  char* prompt=PROMPT;

  while((cmdline = read_cmd(prompt,stdin)) != NULL)
  {
    if((arglist = tokenize(cmdline)) != NULL)
    {
      execute(arglist);

	//free memory
      for(j=0;j<MAXARGS+1;j++)
      free(arglist[j]);
      free(arglist);
      free(cmdline);
    }
  }
  printf("\n");
  return 0;

}
char* read_cmd(char* prompt,FILE* fp)
{
  printf("%s",prompt);
  char* cmdline = (char*)malloc(sizeof(char)*MAX_LEN);
  int c=0;
  int pos = 0;
  while((c=getc(fp)) !=EOF)
  {

    if(c== '\n')
    break;
    cmdline[pos++] =c;
  }
  if(c==EOF && pos==0)
  return NULL;
  cmdline[pos] = '\0';
  return cmdline;
}
char **tokenize(char* cmdline)
{
  char **arglist = (char**)malloc(sizeof(char*)*MAXARGS+1);
  int i;
  for(i=0;i<MAXARGS+1;i++)
  {
    arglist[i] = (char*)malloc(sizeof(char)*ARGLEN);
    bzero(arglist[i],ARGLEN);
  }
  if(cmdline[0]=='\0')
  return NULL;
  char* cp = cmdline;
  char * start;
  int len;
  int argnum = 0;

  while(*cp != '\0')
  {
    while(*cp ==' ' || *cp == '\t')
    cp++;

    start = cp;
    len = 1;
    while(*++cp != '\0' && !(*cp == ' ' || *cp == '\t'))
    len++;

    strncpy(arglist[argnum],start,len);
    arglist[argnum][len] = '\0';

    argnum++;
  }
  arglist[argnum] = NULL;
  return arglist;
}
int execute(char* arglist[])
{
	int rv = RedirectIO(arglist);
	if(rv==0)
	{
		return 0;
	}
  int statusflag=checkIfInternalcmnd(arglist[0]);
  if(statusflag==-1)
  {
    int cpid = fork();
    switch(cpid)
    {

      case -1:
      perror("fork failed");
      exit(1);
      case 0:
      execvp(arglist[0],arglist);
      perror("exec failed");
      exit(1);
      default:
      waitpid(cpid,&statusflag,0);
      printf("child exitted with status %d\n",statusflag>>8);
      return 0;
    }
  }
  else
  {
    if(statusflag==3) printHelp(arglist[1]);
    if(statusflag==1) exit(0);
    if(statusflag==2)
    {
      int rev =  chdir(arglist[1]);
      if(rev==-1)
      {
        fprintf(stderr, "Action cant be  done\n");
      }
    }
  }
}
int checkIfInternalcmnd(char * comand)
{
  if(strcmp(comand,"exit")==0) return 1;
  else if(strcmp(comand,"cd")==0) return 2;
  else if(strcmp(comand,"help")==0) return 3;
  else return -1;
}
void printHelp(char *cmnd)
{
		//exit help
	if(strcmp(cmnd,"exit")==0)
 	printf("exit: exit [n]\n\tExit the shell.\n\n\tExits the shell with a status of N.  If N is omitted, the exit status is that of the last command executed.\n");

	if(strcmp(cmnd,"cd")==0) //cd help
	{
    	printf("cd: cd [-L|[-P [-e]] [-@]] [dir]\n\t\tChange the shell working directory.\n");
    	printf("\t\tChange the current directory to DIR.  The default DIR is the value of the HOME shell variable.\n");
  	}

}
int RedirectIO(char* arglist[])
{

	int i=0;
	for(i=0;arglist[i]!=NULL;i++)
	{
		if(strcmp(arglist[i],"<")==0 || strcmp(arglist[i],"0<")==0)
		{
			
			int saved_stdin = dup(0);
			int saved_stdout = dup(1);
			close(0);
			int fd= open(arglist[i+1],O_RDONLY);	
				for(int j=i;arglist[j]!=NULL;j++)
					{
							if(strcmp(arglist[j],">")==0 || strcmp(arglist[j],"1>")==0)
							{
								close(1);
								int fd2= open(arglist[j+1],O_WRONLY | O_CREAT | O_TRUNC);
							}
					}
			if(fork()==0)
				{
					
					execlp(arglist[0],"cat",NULL);
				}
			else{
				wait(NULL);
				
				dup2(saved_stdin,0);
				dup2(saved_stdout,1);
				close(saved_stdout);
				close(saved_stdin);
				return 0;
			    }
			
			
		}
		else if(strcmp(arglist[i],">")==0 || strcmp(arglist[i],"1>")==0)
		{
			
			int saved_stdin = dup(0);
			int saved_stdout = dup(1);
			close(1);
			int fd= open(arglist[i+1],O_WRONLY| O_CREAT | O_TRUNC);
	
				for(int  j=i;arglist[j]!=NULL;j++)
					{
						if(strcmp(arglist[j],"<")==0 || strcmp(arglist[j],"0<")==0)
							{
								close(0);
								int fd2= open(arglist[j+1],O_RDONLY);
							}
					}
			if(fork()==0)
				{
				
					execlp(arglist[0],"cat",NULL);
				}
			else{
				wait(NULL);
				
				dup2(saved_stdin,0);
				dup2(saved_stdout,1);
				close(saved_stdout);
				close(saved_stdin);
				return 0;
			    }
			
			
		}
	}
	return 1;
}
int pipes(char* args[])
{	

	int filedes[2]; // pos. 0 output, pos. 1 input of the pipe
	int filedes2[2];
	
	int num_cmds = 0;
	
	char *command[256];
	
	pid_t pid;
	
	int err = -1;
	int end = 0;
	int i = 0,j = 0,k = 0,l = 0;

	while (args[l] != NULL){
		if (strcmp(args[l],"|") == 0){
			num_cmds++;
		}
		l++;
	}
	num_cmds++;
	
	if(num_cmds ==1)
	{
		//printf("No pipes found");
		return 1;
	}

	while (args[j] != NULL && end != 1){
		k = 0;

		while (strcmp(args[j],"|") != 0){ // command found
			command[k] = args[j];
			j++;	
			if (args[j] == NULL){

				end = 1;
				k++;
				break;
			}
			k++;
		}

		command[k] = NULL;
		j++;		

		if (i % 2 != 0){
			pipe(filedes); // for odd i
		}else{
			pipe(filedes2); // for even i
		}
		
		pid=fork();
		
		if(pid==-1){			
			if (i != num_cmds - 1){
				if (i % 2 != 0){
					close(filedes[1]); // for odd i
				}else{
					close(filedes2[1]); // for even i
				} 
			}			
			printf("Child process could not be created\n");
			return 0;
		}
		if(pid==0){
			// If we are in the first command
			if (i == 0){
				dup2(filedes2[1], STDOUT_FILENO);
			}
			else if (i == num_cmds - 1){
				if (num_cmds % 2 != 0){ // for odd number of commands
					dup2(filedes[0],STDIN_FILENO);
				}else{ // for even number of commands
					dup2(filedes2[0],STDIN_FILENO);
				}

			}else{ // for odd i
				if (i % 2 != 0){
					dup2(filedes2[0],STDIN_FILENO); 
					dup2(filedes[1],STDOUT_FILENO);
				}else{ // for even i
					dup2(filedes[0],STDIN_FILENO); 
					dup2(filedes2[1],STDOUT_FILENO);					
				} 
			}
			
			if (execvp(command[0],command)==err){
				kill(getpid(),SIGTERM);
			}		
		}
				
		// CLOSING DESCRIPTORS ON PARENT
		if (i == 0){
			close(filedes2[1]);
		}
		else if (i == num_cmds - 1){
			if (num_cmds % 2 != 0){					
				close(filedes[0]);
			}else{					
				close(filedes2[0]);
			}
		}else{
			if (i % 2 != 0){					
				close(filedes2[0]);
				close(filedes[1]);
			}else{					
				close(filedes[0]);
				close(filedes2[1]);
			}
		}
				
		waitpid(pid,NULL,0);
				
		i++;	
	}
	return 0;
}

