#include<stdio.h>
#include<stdlib.h>
#include<unistd.h>
#include<string.h>
#include<sys/wait.h>
#include<sys/types.h>


#define MAX_LEN 512
#define MAXARGS 10
#define ARGLEN 30
#define PROMPT "AZAM:- "

char* read_cmd(char* ,FILE *);
char** tokenize(char*);
int execute(char* arglist[]);

int checkIfInternalcmnd(char*);
void printHelp(char *cmd);

int main()
{

  char *cmdline;
  char** arglist;
  int j;
  char* prompt=PROMPT;

  while((cmdline = read_cmd(prompt,stdin)) != NULL)
  {
    if((arglist = tokenize(cmdline)) != NULL)
    {
      execute(arglist);

	//free memory
      for(j=0;j<MAXARGS+1;j++)
      free(arglist[j]);
      free(arglist);
      free(cmdline);
    }
  }
  printf("\n");
  return 0;

}
char* read_cmd(char* prompt,FILE* fp)
{
  printf("%s",prompt);
  char* cmdline = (char*)malloc(sizeof(char)*MAX_LEN);
  int c=0;
  int pos = 0;
  while((c=getc(fp)) !=EOF)
  {

    if(c== '\n')
    break;
    cmdline[pos++] =c;
  }
  if(c==EOF && pos==0)
  return NULL;
  cmdline[pos] = '\0';
  return cmdline;
}
char **tokenize(char* cmdline)
{
  char **arglist = (char**)malloc(sizeof(char*)*MAXARGS+1);
  int i;
  for(i=0;i<MAXARGS+1;i++)
  {
    arglist[i] = (char*)malloc(sizeof(char)*ARGLEN);
    bzero(arglist[i],ARGLEN);
  }
  if(cmdline[0]=='\0')
  return NULL;
  char* cp = cmdline;
  char * start;
  int len;
  int argnum = 0;

  while(*cp != '\0')
  {
    while(*cp ==' ' || *cp == '\t')
    cp++;

    start = cp;
    len = 1;
    while(*++cp != '\0' && !(*cp == ' ' || *cp == '\t'))
    len++;

    strncpy(arglist[argnum],start,len);
    arglist[argnum][len] = '\0';

    argnum++;
  }
  arglist[argnum] = NULL;
  return arglist;
}
int execute(char* arglist[])
{
  int statusflag=checkIfInternalcmnd(arglist[0]);
  if(statusflag==-1)
  {
    int cpid = fork();
    switch(cpid)
    {

      case -1:
      perror("fork failed");
      exit(1);
      case 0:
      execvp(arglist[0],arglist);
      perror("exec failed");
      exit(1);
      default:
      waitpid(cpid,&statusflag,0);
      printf("child exitted with status %d\n",statusflag>>8);
      return 0;
    }
  }
  else
  {
    if(statusflag==3) printHelp(arglist[1]);
    if(statusflag==1) exit(0);
    if(statusflag==2)
    {
      int rev =  chdir(arglist[1]);
      if(rev==-1)
      {
        fprintf(stderr, "Action cant be  done\n");
      }
    }
  }
}
int checkIfInternalcmnd(char * comand)
{
  if(strcmp(comand,"exit")==0) return 1;
  else if(strcmp(comand,"cd")==0) return 2;
  else if(strcmp(comand,"help")==0) return 3;
  else return -1;
}
void printHelp(char *cmnd)
{
		//exit help
	if(strcmp(cmnd,"exit")==0)
 	printf("exit: exit [n]\n\tExit the shell.\n\n\tExits the shell with a status of N.  If N is omitted, the exit status is that of the last command executed.\n");

	if(strcmp(cmnd,"cd")==0) //cd help
	{
    	printf("cd: cd [-L|[-P [-e]] [-@]] [dir]\n\t\tChange the shell working directory.\n");
    	printf("\t\tChange the current directory to DIR.  The default DIR is the value of the HOME shell variable.\n");
  	}

}

